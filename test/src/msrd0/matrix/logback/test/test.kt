/*
 * matrix-logback-appender
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package msrd0.matrix.logback.test

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.Context
import msrd0.matrix.client.*
import msrd0.matrix.logback.MatrixAppender
import org.slf4j.LoggerFactory
import org.testng.Assert.*
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test
import java.net.URI

class MatrixAppenderTest
{
	companion object
	{
		private val domain = if (System.getenv().containsKey("CI")) "synapse:8008" else "localhost:8008"
		private val hs = HomeServer(domain, URI("http://$domain"))
		private var id = MatrixId("test${System.currentTimeMillis()}", "synapse")
		private val password = "Eish2nies9peifaez7uX"
		
		private lateinit var client : MatrixClient
		private lateinit var room : Room
		
		private lateinit var logger : Logger
		private val message : String get() = "Test Message"
	}
	
	@BeforeTest
	fun setup()
	{
		client = MatrixClient.register(id.localpart, hs, password)
		id = client.id
		room = client.createRoom()
		
		val context = LoggerFactory.getILoggerFactory() as Context
		
		val encoder = PatternLayoutEncoder()
		encoder.pattern = "%msg%n"
		encoder.context = context
		encoder.start()
		
		val appender = MatrixAppender()
		appender.context = context
		appender.encoder = encoder
		appender.matrixId = id.toString()
		appender.homeServerUrl = hs.base.toString()
		appender.deviceId = client.deviceId
		appender.token = client.token
		appender.roomId = room.id.toString()
		appender.start()
		
		logger = LoggerFactory.getLogger(MatrixAppenderTest::class.java) as Logger
		logger.addAppender(appender)
		logger.level = Level.INFO
	}
	
	@Test
	fun test()
	{
		logger.info(message)
		
		// wait until the message is posted
		Thread.sleep(500)
		
		// make sure that we received the message
		assertNotNull(room.retrieveMessages().find { it.content.body.trim() == message })
	}
}
