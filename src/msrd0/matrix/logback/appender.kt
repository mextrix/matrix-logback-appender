/*
 * matrix-logback-appender
 * Copyright (C) 2017 Dominic Meiser
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
 */

package msrd0.matrix.logback

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.UnsynchronizedAppenderBase
import ch.qos.logback.core.encoder.Encoder
import ch.qos.logback.core.status.ErrorStatus
import msrd0.matrix.client.*
import msrd0.matrix.client.event.TextMessageContent
import java.net.URI
import kotlin.text.Charsets.UTF_8

open class MatrixAppender : UnsynchronizedAppenderBase<ILoggingEvent>()
{
	companion object Global
	{
		@JvmStatic
		var globalMatrixClient : MatrixClient? = null
	}
	
	var encoder : Encoder<ILoggingEvent>? = null
	
	var matrixId : String? = null
	var homeServerUrl : String? = null
	var deviceId : String? = null
	var token : String? = null
	
	var roomId : String? = null
	
	private lateinit var client : MatrixClient
	private lateinit var room : Room
	
	override fun start()
	{
		var errors = 0
		
		if (encoder == null)
		{
			addStatus(ErrorStatus("No encoder set for the appender named '$name'.", this))
			errors++
		}
		
		if (matrixId != null && homeServerUrl != null && deviceId != null && token != null)
		{
			val id = MatrixId.fromString(matrixId!!)
			client = MatrixClient(HomeServer(id.domain, URI(homeServerUrl)), id)
			client.userData = MatrixUserData(token!!, deviceId!!)
		}
		else if (globalMatrixClient != null)
			client = globalMatrixClient!!
		else
		{
			addStatus(ErrorStatus("Not enough information to create matrix client and no global matrix client set.", this))
			errors++
		}
		
		if (roomId != null)
			room = Room(client, RoomId.fromString(roomId!!))
		else
		{
			addStatus(ErrorStatus("No room id set for appender named '$name'.", this))
			errors++
		}
		
		if (errors == 0)
			super.start()
	}
	
	override fun append(event : ILoggingEvent?)
	{
		if (event == null)
			return
		
		room.sendMessage(TextMessageContent(String(encoder!!.encode(event), UTF_8)))
	}
}
